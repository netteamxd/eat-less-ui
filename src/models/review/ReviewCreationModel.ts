export class ReviewCreationModel {
  rating = 3;
  comment: string;
  recipeId: number;

  public setComment(comment: string) {
    if (typeof comment === 'undefined' || !comment) {
      throw new Error('Treść komentarza nie może być pusta');
    }
    if (comment.length > 200) {
      throw new Error('Komentarz nie może być dłuższy niż 200 znaków');
    }
    this.comment = comment;
  }

  public setRating(rating: number) {
    if (rating < 1 || rating > 5) {
      throw new Error('Ocena musi być z zakresu od 1 do 5');
    }
    this.rating = rating;
  }

  public setRecipeId(recipeId: number) {
    this.recipeId = recipeId;
  }
}
