import { UserDetailsModel } from '../user/UserDetailsModel';

export class ReviewDetailsModel {
  id: number;
  rating: number;
  comment: string;
  author: UserDetailsModel;
}
