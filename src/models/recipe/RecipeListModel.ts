export class RecipeListModel {
  id: number;
  name: string;
  image: string;
  totalCalories: number;
  portions: number;
  preparationTime: number;
  averageRating: number;
}
