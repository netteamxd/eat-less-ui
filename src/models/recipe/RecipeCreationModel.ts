import { IngredientForRecipeCreationModel } from '../ingredient/IngredientForRecipeCreationModel';
import { CategoryForRecipeCreationModel } from '../category/CategoryForRecipeCreationModel';

export class RecipeCreationModel {
  name: string;
  image: string;
  content: string;
  totalCalories: number;
  portions: number;
  preparationTime: number;
  categories: CategoryForRecipeCreationModel[] = [];
  ingredients: IngredientForRecipeCreationModel[] = [];

  setName(name: string) {
    if (typeof name === 'undefined' || !name) {
      throw new Error('Nazwa przepisu nie może być pusta.');
    }
    if (name.length > 100) {
      throw new Error('Nazwa przepisu nie może dłuższa niż 100 znaków.');
    }
    this.name = name;
  }

  setContent(content: string) {
    if (typeof content === 'undefined' || !content) {
      throw new Error('Treść przepisu nie może być pusta.');
    }
    if (content.length > 10000) {
      throw new Error('Treść przepisu nie może dłuższa niż 10000 znaków.');
    }
    this.content = content;
  }

  setTotalCalories(totalCalories: number) {
    if (totalCalories < 0) {
      throw new Error('Ilość kalorii nie może być mniejsza niż 0.');
    }
    this.totalCalories = totalCalories;
  }

  setPortions(portions: number) {
    if (portions < 1) {
      throw new Error('Ilość porcji nie może być mniejsza niż 1.');
    }
    this.portions = portions;
  }

  setPreparationTime(preparationTime: number) {
    if (preparationTime < 1) {
      throw new Error('Czas przygotowania nie może być mniejszy niż 1 minuta.');
    }
    this.preparationTime = preparationTime;
  }

  setCategories(selectedCategories: number[]) {
    if (selectedCategories.length === 0) {
      throw new Error('Należy wybrać przynajmniej jedną kategorię.');
    }
    for (const category of selectedCategories) {
      const categoryToAdd = new CategoryForRecipeCreationModel();
      categoryToAdd.id = category;
      this.categories.push(categoryToAdd);
    }
  }

  setIngredients(ingredientsToAdd: IngredientForRecipeCreationModel[]) {
    if (ingredientsToAdd.length === 0) {
      throw new Error('Należy wybrać przynajmniej jeden składnik.');
    }
    for (const ingredient of ingredientsToAdd) {
      if (typeof ingredient.ingredientId === 'undefined' || !ingredient.ingredientId) {
        throw new Error('Należy wybrać składnik.');
      }
      if (typeof ingredient.amount === 'undefined' || !ingredient.amount) {
        throw new Error('Ilość składnika nie może być pusta.');
      }
      if (this.ingredients.find(i => i.ingredientId === ingredient.ingredientId)) {
        throw new Error('Składnik należy wybrać tylko raz.');
      }
      this.ingredients.push(ingredient);
    }
  }

  setImage(image: string) {
    if (image.length === 0) {
        throw new Error('Należy wybrać zdjęcie dla przepisu');
    }
    this.image = image;
  }
}
