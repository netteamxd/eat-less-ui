import { UserDetailsModel } from '../user/UserDetailsModel';
import { CategoryListModel } from '../category/CategoryListModel';
import { IngredientForRecipeDetailsModel } from '../ingredient/IngredientForRecipeDetailsModel';
import { ReviewDetailsModel } from '../review/ReviewDetailsModel';

export class RecipeDetailsModel {
  id: number;
  author: UserDetailsModel;
  name: string;
  content: string;
  image: string;
  totalCalories: number;
  portions: number;
  preparationTime: number;
  isFavourite: boolean;
  categories: CategoryListModel[] = [];
  ingredients: IngredientForRecipeDetailsModel[] = [];
  reviews: ReviewDetailsModel[] = [];
}
