export class UserDetailsModel {
  id: number;
  username: string;
  admin: boolean;
}
