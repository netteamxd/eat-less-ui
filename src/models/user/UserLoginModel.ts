export class UserLoginModel {
  username: string;
  password: string;

  setUsername(username: string) {
    if (typeof username === 'undefined' || !username) {
      throw new Error('Login nie może być pusty.');
    }
    this.username = username;
  }

  setPassword(password: string) {
    if (typeof password === 'undefined' || !password) {
      throw new Error('Hasło nie może być puste.');
    }
    this.password = password;
  }
}
