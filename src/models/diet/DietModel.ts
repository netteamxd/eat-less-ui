import { MealType } from './MealType';
import { RecipeListModel } from '../recipe/RecipeListModel';

export class DietModel {
  meals: Map<MealType, RecipeListModel>;
  totalCalories: number;
}
