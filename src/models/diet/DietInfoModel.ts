import { Gender } from './GenderEnum';
import { Activity } from './ActivityEnum';

export class DietInfoModel {
  mass: number;
  height: number;
  age: number;
  gender: Gender;
  activity: Activity;

  setMass(mass: number) {
    if (typeof mass === 'undefined' || !mass) {
      throw new Error('Należy określić wagę.');
    }
    if (mass < 1) {
      throw new Error('Waga nie może być mniejsza niż 1.');
    }
    this.mass = mass;
  }

  setHeight(height: number) {
    if (typeof height === 'undefined' || !height) {
      throw new Error('Należy określić wzrost.');
    }
    if (height < 1) {
      throw new Error('Wzrost nie może być mniejszy niż 1.');
    }
    this.height = height;
  }

  setAge(age: number) {
    if (typeof age === 'undefined' || !age) {
      throw new Error('Należy określić wiek.');
    }
    if (age < 1) {
      throw new Error('Wiek nie może być mniejszy niż 1.');
    }
    this.age = age;
  }

  setActivity(activity: Activity) {
    if (typeof activity === 'undefined' || !activity) {
      throw new Error('Należy określić aktywność.');
    }
    this.activity = activity;
  }

  setGender(gender: Gender) {
    if (typeof gender === 'undefined' || !gender) {
      throw new Error('Należy określić płeć.');
    }
    this.gender = gender;
  }
}
