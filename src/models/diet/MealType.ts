export enum MealType {
  BREAKFAST,
  SECOND_BREAKFAST,
  DINNER,
  TEA,
  SUPPER
}
