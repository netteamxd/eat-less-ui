export class IngredientListModel {
  id: number;
  ingredientName: string;
  animalProduct: boolean;
}
