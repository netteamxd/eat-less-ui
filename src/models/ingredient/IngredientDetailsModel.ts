export class IngredientDetailsModel {
  id: number;
  ingredientName: string;
  animalProduct: boolean;
}
