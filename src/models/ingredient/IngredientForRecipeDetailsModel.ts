import { IngredientDetailsModel } from './IngredientDetailsModel';

export class IngredientForRecipeDetailsModel {
  ingredient: IngredientDetailsModel;
  amount: string;
}
