export class IngredientUpdateModel {
  id: number;
  ingredientName: string;
  animalProduct: boolean;

  setId(id: number) {
    this.id = id;
  }

  setIngredientName(ingredientName: string) {
    if (typeof ingredientName === 'undefined' || !ingredientName || ingredientName.length < 2) {
      throw new Error('Nazwa składnika nie może być krótsza niż 2 znaki.');
    }
    if (ingredientName.length > 100) {
      throw new Error('Nazwa składnika nie może dłuższa niż 100 znaków.');
    }
    this.ingredientName = ingredientName;
  }

  setIsAnimalProduct(isAnimalProduct: boolean) {
    this.animalProduct = isAnimalProduct;
  }
}
