export class SearchModel {
  maxCalories: number;
  minCalories: number;
  ingredients: number[] = [];
  excluded: number[] = [];
  nameKeywords: string[] = [];
  categories: number[] = [];
}
