import { CategoryListModel } from './CategoryListModel';

export class CategoryDetailsModel {
  id: number;
  categoryName: string;
  parentCategoryId: number;
  childCategories: CategoryListModel[] = [];
}
