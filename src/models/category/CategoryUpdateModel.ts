export class CategoryUpdateModel {
  id: number;
  categoryName: string;
  parentCategoryId: number;

  setCategoryName(categoryName: string) {
    if (typeof categoryName === 'undefined' || !categoryName || categoryName.length < 2) {
      throw new Error('Nazwa kategorii nie może być krótsza niż 2 znaki.');
    }
    if (categoryName.length > 100) {
      throw new Error('Nazwa kategorii nie może dłuższa niż 100 znaków.');
    }
    this.categoryName = categoryName;
  }

  setParentCategory(parentCategoryId: number) {
    this.parentCategoryId = parentCategoryId;
  }

  setCategoryId(id: number) {
    this.id = id;
  }
}
