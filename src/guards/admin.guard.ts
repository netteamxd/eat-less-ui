import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const isAdminLocalStorage = localStorage.getItem('admin');
    if (isAdminLocalStorage !== null && isAdminLocalStorage === 'true') {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
