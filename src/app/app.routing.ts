import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from '@angular/compiler/src/core';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { RecipesListComponent } from './components/recipe/recipes-list/recipes-list.component';
import { RecipeAddComponent } from './components/recipe/recipe-add/recipe-add.component';
import { RecipeDetailsComponent } from './components/recipe/recipe-details/recipe-details.component';
import { RecipeEditComponent } from './components/recipe/recipe-edit/recipe-edit.component';
import { IngredientsListComponent } from './components/ingredient/ingredients-list/ingredients-list.component';
import { CategoriesListComponent } from './components/category/categories-list/categories-list.component';
import { CategoryDetailsComponent } from './components/category/category-details/category-details.component';
import { SearchComponent } from './components/search/search/search.component';
import { SearchAdvancedComponent } from './components/search/search-advanced/search-advanced.component';
import { GenerateDietComponent } from './components/diet/generate-diet/generate-diet.component';
import { SettingsComponent } from './components/profile/settings/settings.component';
import { AuthGuard } from '../guards/auth.guard';
import { AdminGuard } from '../guards/admin.guard';
import { CategoryViewListComponent } from './components/category/category-view-list/category-view-list.component';

export const AppRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'recipes', component: RecipesListComponent},
  { path: 'recipes/add', component: RecipeAddComponent, canActivate: [AuthGuard]},
  { path: 'recipes/:id', component: RecipeDetailsComponent},
  { path: 'recipes/:id/edit', component: RecipeEditComponent, canActivate: [AuthGuard]},
  { path: 'ingredients', component: IngredientsListComponent, canActivate: [AdminGuard]},
  { path: 'categoriesPanel', component: CategoriesListComponent, canActivate: [AdminGuard]},
  { path: 'categories/:id', component: CategoryDetailsComponent},
  { path: 'categories', component: CategoryViewListComponent},
  { path: 'search/:keywords', component: SearchComponent},
  { path: 'searchAdvanced', component: SearchAdvancedComponent},
  { path: 'profile/settings', component: SettingsComponent, canActivate: [AuthGuard]},
  { path: 'diet/generate', component: GenerateDietComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
