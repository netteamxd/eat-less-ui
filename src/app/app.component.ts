import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgcCookieConsentService } from 'ngx-cookieconsent';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public constructor(private titleService: Title, private ccService: NgcCookieConsentService ) {
    titleService.setTitle( 'Eat Less' );
  }

  onDeactivate() {
    document.body.scrollTop = 0;
  }
}
