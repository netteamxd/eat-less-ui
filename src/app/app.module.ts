import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppBootstrapModule } from './app-bootstrap.module';
import { HomeComponent } from './components/home/home.component';
import { NavmenuComponent } from './components/navmenu/navmenu.component';
import { AuthService } from '../services/auth.service';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { AlertComponent } from './components/alert/alert.component';
import { AlertService } from '../services/alert.service';
import { RecipesListComponent } from './components/recipe/recipes-list/recipes-list.component';
import { RecipeService } from '../services/recipe.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { RecipeAddComponent } from './components/recipe/recipe-add/recipe-add.component';
import { CategoryService } from '../services/category.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { IngredientService } from '../services/ingredient.service';
import { ImageService } from '../services/image.service';
import { RecipeDetailsComponent } from './components/recipe/recipe-details/recipe-details.component';
import { RecipeEditComponent } from './components/recipe/recipe-edit/recipe-edit.component';
import { UserService } from '../services/user.service';
import { ReviewService } from '../services/review.service';
import { IngredientsListComponent } from './components/ingredient/ingredients-list/ingredients-list.component';
import { CategoriesListComponent } from './components/category/categories-list/categories-list.component';
import { CategoryDetailsComponent } from './components/category/category-details/category-details.component';
import { RecipeListItemComponent } from './components/recipe/recipe-list-item/recipe-list-item.component';
import { SearchComponent } from './components/search/search/search.component';
import { SearchAdvancedComponent } from './components/search/search-advanced/search-advanced.component';
import { SettingsComponent } from './components/profile/settings/settings.component';
import { GenerateDietComponent } from './components/diet/generate-diet/generate-diet.component';
import { DietService } from '../services/diet.service';
import { AuthGuard } from '../guards/auth.guard';
import { AdminGuard } from '../guards/admin.guard';
import { ErrorInterceptor } from '../interceptors/error.interceptor';
import { CategoryViewListComponent } from './components/category/category-view-list/category-view-list.component';
import { NgcCookieConsentConfig, NgcCookieConsentModule } from 'ngx-cookieconsent';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'localhost'
  },
  content: {
    message: 'Strona zawiera ciastka. Niestety nie nadają się one do spożycia ☹',
    dismiss: 'Rozumiem',
    link: 'Dowiedz się więcej'
  },
  palette: {
    popup: {
      background: '#edeff5',
      text: '#838391'
    },
    button: {
      background: '#4b81e8'
    }
  },
  position: 'bottom-left',
  theme: 'block',
  type: 'info'
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavmenuComponent,
    RegisterComponent,
    LoginComponent,
    AlertComponent,
    RecipesListComponent,
    RecipeAddComponent,
    RecipeDetailsComponent,
    RecipeEditComponent,
    IngredientsListComponent,
    CategoriesListComponent,
    CategoryDetailsComponent,
    RecipeListItemComponent,
    SearchComponent,
    SearchAdvancedComponent,
    GenerateDietComponent,
    SettingsComponent,
    CategoryViewListComponent
  ],
  imports: [
    BrowserModule,
    routing,
    NgSelectModule,
    FormsModule,
    HttpClientModule,
    AppBootstrapModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    NgcCookieConsentModule.forRoot(cookieConfig)
  ],
  providers: [
    AuthService,
    AlertService,
    RecipeService,
    CategoryService,
    IngredientService,
    ImageService,
    UserService,
    ReviewService,
    DietService,
    AuthGuard,
    AdminGuard,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
