import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../../../services/recipe.service';
import { RecipeListModel } from '../../../../models/recipe/RecipeListModel';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
  currentPage = 1;
  recipes: RecipeListModel[] = [];

  constructor(private recipeService: RecipeService, private alertService: AlertService) { }

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes() {
    this.recipeService.getAll().subscribe(
      data => this.recipes = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania listy przepisów.')
    );
  }
}
