import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../../../services/recipe.service';
import { AlertService } from '../../../../services/alert.service';
import { RecipeCreationModel } from '../../../../models/recipe/RecipeCreationModel';
import { CategoryService } from '../../../../services/category.service';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';
import { Observable } from 'rxjs';
import { IngredientListModel } from '../../../../models/ingredient/IngredientListModel';
import { IngredientService } from '../../../../services/ingredient.service';
import { IngredientForRecipeCreationModel } from '../../../../models/ingredient/IngredientForRecipeCreationModel';
import { Router } from '@angular/router';
import { ImageService } from '../../../../services/image.service';

@Component({
  selector: 'app-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.css']
})
export class RecipeAddComponent implements OnInit {
  protected image: File = null;
  protected ingredients: IngredientListModel[];
  protected categories$: Observable<CategoryListModel[]>;
  protected selectedCategories = [];
  protected tempRecipe: RecipeCreationModel = new RecipeCreationModel();
  constructor(private recipeService: RecipeService, private alertService: AlertService,
              private categoryService: CategoryService, private ingredientService: IngredientService,
              private router: Router, private imageService: ImageService) { }

  ngOnInit() {
    this.tempRecipe.setPortions(1);
    this.tempRecipe.setPreparationTime(1);
    this.tempRecipe.setTotalCalories(0);
    this.categories$ = this.categoryService.getLowestCategories();
    this.ingredientService.getAll().subscribe(data => this.ingredients = data);
    this.tempRecipe.ingredients.push(new IngredientForRecipeCreationModel());
  }

  handleFileInput(files: FileList) {
    this.image = files.item(0);
  }

  add() {
    const recipe = new RecipeCreationModel();
    try {
      recipe.setName(this.tempRecipe.name);
      recipe.setContent(this.tempRecipe.content);
      recipe.setPortions(this.tempRecipe.portions);
      recipe.setTotalCalories(this.tempRecipe.totalCalories);
      recipe.setPreparationTime(this.tempRecipe.preparationTime);
      recipe.setCategories(this.selectedCategories);
      recipe.setIngredients(this.tempRecipe.ingredients);
    } catch (e) {
      this.alertService.error(e.message);
      return;
    }

    if (this.image == null) {
      this.alertService.error('Należy wybrać zdjęcie');
    }

    this.imageService.uploadImage(this.image).subscribe(
      data => {
        try {
            recipe.setImage(data);
            this.postRecipe(recipe);
          } catch (e) {
            this.alertService.error('Wystąpił nieoczekiwany błąd podczas dodawania przepisu.');
            return;
          }
      },
      error => {
        this.alertService.error('Wystąpił nieoczekiwany błąd podczas dodawania przepisu.');
      }
    );
  }

  postRecipe(recipe) {
    this.recipeService.add(recipe).subscribe(
      data => {
        this.alertService.success('Pomyślnie dodano przepis.', true);
        this.router.navigate(['/recipes']);
      },
      error => {
        this.alertService.error('Wystąpił nieoczekiwany błąd podczas dodawania przepisu.');
      }
    );
  }

  addIngredient() {
    this.tempRecipe.ingredients.push(new IngredientForRecipeCreationModel());
  }

  removeIngredient(index: number) {
    this.tempRecipe.ingredients.splice(index, 1);
  }
}
