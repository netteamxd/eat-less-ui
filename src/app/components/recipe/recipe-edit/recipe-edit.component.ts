import { Component, OnInit } from '@angular/core';
import { RecipeDetailsModel } from '../../../../models/recipe/RecipeDetailsModel';
import { RecipeService } from '../../../../services/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../services/alert.service';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';
import { Observable } from 'rxjs/index';
import { IngredientListModel } from '../../../../models/ingredient/IngredientListModel';
import { IngredientForRecipeCreationModel } from '../../../../models/ingredient/IngredientForRecipeCreationModel';
import { RecipeCreationModel } from '../../../../models/recipe/RecipeCreationModel';
import { IngredientService } from '../../../../services/ingredient.service';
import { CategoryService } from '../../../../services/category.service';
import { RecipeUpdateModel } from '../../../../models/recipe/RecipeUpdateModel';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  protected recipeId: number;
  protected recipeDetails: RecipeDetailsModel;
  protected tempRecipe: RecipeCreationModel = new RecipeCreationModel();
  protected ingredients: IngredientListModel[];
  protected categories$: Observable<CategoryListModel[]>;
  protected selectedCategories = [];

  constructor(private recipeService: RecipeService, private route: ActivatedRoute,
              private alertService: AlertService, private categoryService: CategoryService,
              private ingredientService: IngredientService, private router: Router) { }

  ngOnInit() {
    this.recipeId = this.route.snapshot.params['id'];
    this.getRecipe(this.recipeId);
    this.categories$ = this.categoryService.getLowestCategories();
    this.ingredientService.getAll().subscribe(data => this.ingredients = data);
  }

  getRecipe(id: number) {
    this.recipeService.getDetails(id).subscribe(
      data => {this.recipeDetails = data; this.setValues(); },
      error => {this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania przepisu'); return; }
    );
  }

  addIngredient() {
    this.tempRecipe.ingredients.push(new IngredientForRecipeCreationModel());
  }

  removeIngredient(index: number) {
    this.tempRecipe.ingredients.splice(index, 1);
  }

  setValues() {
    this.tempRecipe.setName(this.recipeDetails.name);
    this.tempRecipe.setContent(this.recipeDetails.content);
    this.tempRecipe.setPortions(this.recipeDetails.portions);
    this.tempRecipe.setTotalCalories(this.recipeDetails.totalCalories);
    this.tempRecipe.setPreparationTime(this.recipeDetails.preparationTime);
    this.tempRecipe.setImage(this.recipeDetails.image);
    this.selectedCategories = this.recipeDetails.categories.map(c => c.id);
    const ingredients: IngredientForRecipeCreationModel[] = this.recipeDetails.ingredients
      .map(function (c) {
         const ingredient = new IngredientForRecipeCreationModel();
         ingredient.ingredientId = c.ingredient.id;
         ingredient.amount = c.amount;
         return ingredient;
    });
  this.tempRecipe.setIngredients(ingredients);
  }

  save() {
    const recipe = new RecipeUpdateModel();
    try {
      recipe.setId(this.recipeId);
      recipe.setName(this.tempRecipe.name);
      recipe.setContent(this.tempRecipe.content);
      recipe.setPortions(this.tempRecipe.portions);
      recipe.setTotalCalories(this.tempRecipe.totalCalories);
      recipe.setPreparationTime(this.tempRecipe.preparationTime);
      recipe.setCategories(this.selectedCategories);
      recipe.setIngredients(this.tempRecipe.ingredients);
      recipe.setImage(this.tempRecipe.image);
    } catch (e) {
      this.alertService.error(e.message);
      return;
    }

    this.recipeService.update(recipe).subscribe(
      data => {
        this.alertService.success('Pomyślnie edytowano przepis.', true);
        this.router.navigate(['/recipes/' + this.recipeId]);
      },
      error => {
        this.alertService.error('Wystąpił nieoczekiwany błąd podczas zapisu przepisu.');
      }
    );

  }
}
