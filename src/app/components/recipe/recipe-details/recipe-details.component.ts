import { ChangeDetectorRef, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { RecipeDetailsModel } from '../../../../models/recipe/RecipeDetailsModel';
import { RecipeService } from '../../../../services/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../services/alert.service';
import { ReviewCreationModel } from '../../../../models/review/ReviewCreationModel';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ReviewService } from '../../../../services/review.service';
import { ReviewDetailsModel } from '../../../../models/review/ReviewDetailsModel';
import { ReviewUpdateModel } from '../../../../models/review/ReviewUpdateModel';
import { UserService } from '../../../../services/user.service';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  @ViewChild('commentEditTextArea') commentTextArea: ElementRef;
  addToFavHovered = false;
  commentToDeleteId: number;
  modalRef: BsModalRef;
  $isAuthenticated: boolean;
  $isAdmin: boolean;
  protected recipeId: number;
  protected reviewCreateResult: any;
  protected tempCreationReview: ReviewCreationModel = new ReviewCreationModel();
  protected reviewUpdateResult: any;
  protected tempUpdateReview: ReviewUpdateModel = new ReviewUpdateModel();
  protected recipe: RecipeDetailsModel = null;

  constructor(private authService: AuthService, private recipeService: RecipeService, private route: ActivatedRoute,
              private alertService: AlertService, private router: Router,
              private modalService: BsModalService, private reviewService: ReviewService,
              private changeDetector: ChangeDetectorRef, private userService: UserService) { }

  ngOnInit() {
    this.recipeId = this.route.snapshot.params['id'];
    this.getRecipe(this.recipeId);
    this.authService.isAuthenticated.subscribe(state => this.$isAuthenticated = state);
    this.authService.isAdmin.subscribe(state => this.$isAdmin = state);
  }

  isAdminOrOwner(owner: string) {
    const username = localStorage.getItem('username');
    const isAdmin = localStorage.getItem('admin');
    return (username !== null && username === owner) || (isAdmin != null && isAdmin === 'true');
  }

  getRecipe(id: number) {
    this.recipeService.getDetails(id).subscribe(
      data => this.recipe = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania szczegółów przepisu')
    );
  }

  addReview() {
    const review = new ReviewCreationModel();
    try {
      review.setComment(this.tempCreationReview.comment);
      review.setRating(this.tempCreationReview.rating);
      review.setRecipeId(this.recipeId);
    } catch (e) {
      this.reviewCreateResult = { type: 'error', text: e.message };
      return;
    }

    this.reviewService.add(review).subscribe(
      data => {
        const reviewDetails = <ReviewDetailsModel>data;
        this.reviewCreateResult = { type: 'success', text: 'Komentarz pomyślnie dodany' };
        this.recipe.reviews.push(reviewDetails);
      },
    error => this.reviewCreateResult = { type: 'error', text: 'Wystąpił nieoczekiwany błąd podczas dodawania komentarza' }
    );
  }

  deleteRecipe() {
    this.modalRef.hide();
    this.recipeService.delete(this.recipeId).subscribe(
      data => this.router.navigate(['/recipes']),
      error => this.alertService.error('Usunięcie przepisu nie powiodło się.')
    );
  }

  deleteComment() {
    this.modalRef.hide();
    this.reviewService.delete(this.commentToDeleteId).subscribe(
      data => {
        const index = this.recipe.reviews.findIndex(r => r.id === this.commentToDeleteId);
        this.recipe.reviews.splice(index, 1);
      }
    );
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  openCommentModal(template: TemplateRef<any>, id: number) {
    this.modalRef = this.modalService.show(template);
    this.commentToDeleteId = id;
  }

  editComment(id: number) {
    const reviewDuringEdition = this.recipe.reviews.find(r => r.id === id);
    this.tempUpdateReview.id = id;
    this.tempUpdateReview.comment = reviewDuringEdition.comment;
    this.tempUpdateReview.rating = reviewDuringEdition.rating;
    this.changeDetector.detectChanges();
    this.commentTextArea.nativeElement.focus();
  }

  cancelCommentEdit() {
    this.tempUpdateReview.id = 0;
  }

  saveCommentEdit() {
    const review = new ReviewUpdateModel();
    try {
      review.setComment(this.tempUpdateReview.comment);
      review.setRating(this.tempUpdateReview.rating);
      review.setId(this.tempUpdateReview.id);
    } catch (e) {
      this.reviewUpdateResult = e.message;
      return;
    }

    this.reviewService.update(review).subscribe(
      data => {
        const reviewDetails = <ReviewDetailsModel>data;
        const editedReviewIndex = this.recipe.reviews.findIndex(r => r.id === reviewDetails.id);
        this.recipe.reviews[editedReviewIndex] = reviewDetails;
        this.tempUpdateReview = new ReviewUpdateModel();
        this.reviewUpdateResult = null;
      },
      error => this.reviewUpdateResult = 'Wystąpił nieoczekiwany błąd podczas edycji komentarza'
    );
  }

  addFavourite() {
    this.userService.addFavourite(this.recipe.id).subscribe(
      data => this.recipe.isFavourite = true,
      error => this.alertService.error('Wystąpił błąd podczas dodawania przepisu do ulubionych')
    );
  }

  removeFavourite() {
    this.userService.removeFavourite(this.recipe.id).subscribe(
      data => this.recipe.isFavourite = false,
      error => this.alertService.error('Wystąpił błąd podczas usuwania z ulubionych')
    );
  }
}

