import { Component, Input, OnInit } from '@angular/core';
import { RecipeListModel } from '../../../../models/recipe/RecipeListModel';

@Component({
  selector: 'app-recipe-list-item',
  templateUrl: './recipe-list-item.component.html',
  styleUrls: ['./recipe-list-item.component.css']
})
export class RecipeListItemComponent implements OnInit {
  @Input() recipe: RecipeListModel = null;
  constructor() { }

  ngOnInit() {
  }

  getStars(rating: number) {
    const size = rating / 5 * 100;
    return size + '%';
  }
}
