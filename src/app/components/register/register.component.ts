import { Component, OnInit } from '@angular/core';
import { UserRegistrationModel } from '../../../models/user/UserRegistrationModel';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  tempUser: UserRegistrationModel = new UserRegistrationModel();
  constructor(private userService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
  }

  register() {
    const newUser = new UserRegistrationModel();

    try {
      newUser.setUsername(this.tempUser.username);
      newUser.setPassword(this.tempUser.password);
    } catch (e) {
      this.alertService.error(e.message);
      return;
    }

    this.userService.register(newUser);
  }
}
