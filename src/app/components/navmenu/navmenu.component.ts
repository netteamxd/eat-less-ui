import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavmenuComponent implements OnInit {
  nameKeywords = '';
  $isAuthenticated: boolean;
  $isAdmin: boolean;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.isAuthenticated.subscribe(state => this.$isAuthenticated = state);
    this.authService.isAdmin.subscribe(state => this.$isAdmin = state);
  }

  logout() {
    this.authService.logout();
  }

  search() {
    const keywords = this.nameKeywords.split(' ').join(',');
    this.nameKeywords = '';
    this.router.navigate(['/search', keywords]);
  }
}
