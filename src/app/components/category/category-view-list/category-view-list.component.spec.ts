import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryViewListComponent } from './category-view-list.component';

describe('CategoryViewListComponent', () => {
  let component: CategoryViewListComponent;
  let fixture: ComponentFixture<CategoryViewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryViewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryViewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
