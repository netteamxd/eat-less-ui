import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-category-view-list',
  templateUrl: './category-view-list.component.html',
  styleUrls: ['./category-view-list.component.css']
})
export class CategoryViewListComponent implements OnInit {
  categories: CategoryListModel[] = [];
  constructor(private categoryService: CategoryService, private alertService: AlertService) { }

  ngOnInit() {
    this.categoryService.getAll().subscribe(
      data => this.categories = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania listy kategorii')
    );
  }

}
