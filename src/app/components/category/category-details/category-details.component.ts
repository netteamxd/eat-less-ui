import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { AlertService } from '../../../../services/alert.service';
import { CategoryDetailsModel } from '../../../../models/category/CategoryDetailsModel';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { RecipeListModel } from '../../../../models/recipe/RecipeListModel';
import { RecipeService } from '../../../../services/recipe.service';
import { SearchModel } from '../../../../models/search/SearchModel';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {
  currentPage = 1;
  categoryId: number;
  category: CategoryDetailsModel = null;
  categoryGenealogy: CategoryListModel[] = [];
  recipes: RecipeListModel[] = [];
  constructor(private categoryService: CategoryService, private alertService: AlertService,
              private route: ActivatedRoute, private recipeService: RecipeService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.categoryId = parseInt(params.get('id'), 10);
      this.getCategory();
    });
  }

  getCategory() {
    this.categoryService.getDetails(this.categoryId).subscribe(
      data => {
        this.category = data;
        this.recipes = [];
        this.getGenealogy();
        if (data.childCategories.length === 0) {
          this.getRecipes();
        }
      },
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania szczegółów kategorii')
    );
  }

  getRecipes() {
    const search = new SearchModel();
    search.categories.push(this.categoryId);
    this.recipeService.search(search).subscribe(
      data => this.recipes = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania szczegółów kategorii')
    );
  }

  getGenealogy() {
    this.categoryService.getCategoryGenealogy(this.categoryId).subscribe(
      data => this.categoryGenealogy = data
    );
  }

}
