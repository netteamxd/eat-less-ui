import { Component, OnInit, TemplateRef } from '@angular/core';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';
import { CategoryService } from '../../../../services/category.service';
import { AlertService } from '../../../../services/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CategoryCreationModel } from '../../../../models/category/CategoryCreationModel';
import { CategoryUpdateModel } from '../../../../models/category/CategoryUpdateModel';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {
  currentPage = 1;
  categories: CategoryListModel[] = [];
  categoriesForUpdate: CategoryListModel[] = [];
  modalRef: BsModalRef;
  categoryCreateResult: any;
  tempCategoryCreation: CategoryCreationModel = new CategoryCreationModel();
  categoryUpdateResult: any;
  tempCategoryUpdate: CategoryUpdateModel = new CategoryUpdateModel();
  constructor(private categoryService: CategoryService, private alertService: AlertService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.categoryService.getAll().subscribe(
      data => this.categories = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania listy kategorii')
    );
  }

  openCategoryEditModal(template: TemplateRef<any>, categoryId: number) {
    const categoryDuringEdition = this.categories.find(c => c.id === categoryId);
    this.tempCategoryUpdate.id = categoryDuringEdition.id;
    this.tempCategoryUpdate.categoryName = categoryDuringEdition.categoryName;
    this.tempCategoryUpdate.parentCategoryId = categoryDuringEdition.parentCategoryId;
    this.categoriesForUpdate = this.categories.filter(c => c.id !== categoryId);
    this.modalRef = this.modalService.show(template);
  }

  openCategoryAddModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  addCategory() {
    const category = new CategoryCreationModel();
    try {
      category.setCategoryName(this.tempCategoryCreation.categoryName);
      category.setParentCategory(this.tempCategoryCreation.parentCategoryId);
    } catch (e) {
      this.categoryCreateResult = e.message;
      return;
    }

    this.categoryService.add(category).subscribe(
      data => {
        const categoryForList = new CategoryListModel();
        categoryForList.categoryName = data.categoryName;
        categoryForList.id = data.id;
        this.categories.push(categoryForList);
        this.categoryCreateResult = null;
        this.tempCategoryCreation = new CategoryCreationModel();
        this.modalRef.hide();
      },
      error => {
        this.categoryCreateResult = 'Wystąpił nieoczekiwany błąd podczas dodawania kategorii';
      }
    );
  }

  updateCategory() {
    const category = new CategoryUpdateModel();
    try {
      category.setCategoryId(this.tempCategoryUpdate.id);
      category.setCategoryName(this.tempCategoryUpdate.categoryName);
      category.setParentCategory(this.tempCategoryUpdate.parentCategoryId);
    } catch (e) {
      this.categoryUpdateResult = e.message;
      return;
    }

    this.categoryService.update(category).subscribe(
      data => {
        const editedCategory = this.categories.find(c => c.id === data.id);
        editedCategory.categoryName = data.categoryName;
        editedCategory.id = data.id;
        editedCategory.parentCategoryId = data.parentCategoryId;
        this.categoryUpdateResult = null;
        this.tempCategoryUpdate = new CategoryUpdateModel();
        this.modalRef.hide();
      },
      error => {
        this.categoryUpdateResult = 'Wystąpił nieoczekiwany błąd podczas edycji kategorii';
      }
    );
  }
}
