import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../../services/recipe.service';
import { AlertService } from '../../../services/alert.service';
import { RecipeListModel } from '../../../models/recipe/RecipeListModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  recipes: RecipeListModel[] = [];
  constructor(private recipeService: RecipeService, private alertService: AlertService) { }

  ngOnInit() {
    this.getRecommended();
  }

  getRecommended() {
    this.recipeService.getRecommended().subscribe(
      data => this.recipes = data
    );
  }

}
