import { Component, OnInit, TemplateRef } from '@angular/core';
import { IngredientListModel } from '../../../../models/ingredient/IngredientListModel';
import { IngredientService } from '../../../../services/ingredient.service';
import { AlertService } from '../../../../services/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { IngredientCreationModel } from '../../../../models/ingredient/IngredientCreationModel';
import { IngredientUpdateModel } from '../../../../models/ingredient/IngredientUpdateModel';

@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.css']
})
export class IngredientsListComponent implements OnInit {
  currentPage = 1;
  ingredients: IngredientListModel[] = [];
  modalRef: BsModalRef;
  ingredientCreateResult: any;
  tempIngredientCreation: IngredientCreationModel = new IngredientCreationModel();
  ingredientUpdateResult: any;
  tempIngredientUpdate: IngredientUpdateModel = new IngredientUpdateModel();

  constructor(private ingredientService: IngredientService, private alertService: AlertService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.ingredientService.getAll().subscribe(
      data => this.ingredients = data,
      error => this.alertService.error('Wystąpił nieoczekiwany błąd podczas pobierania listy składników')
    );
  }

  openIngredientEditModal(template: TemplateRef<any>, ingredientId: number) {
    const ingredientDuringEdition = this.ingredients.find(i => i.id === ingredientId);
    this.tempIngredientUpdate.id = ingredientDuringEdition.id;
    this.tempIngredientUpdate.ingredientName = ingredientDuringEdition.ingredientName;
    this.tempIngredientUpdate.animalProduct = ingredientDuringEdition.animalProduct;
    this.modalRef = this.modalService.show(template);
  }

  openIngredientAddModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  addIngredient() {
    const ingredient = new IngredientCreationModel();
    try {
      ingredient.setIngredientName(this.tempIngredientCreation.ingredientName);
      ingredient.setIsAnimalProduct(this.tempIngredientCreation.animalProduct);
    } catch (e) {
      this.ingredientCreateResult = e.message;
      return;
    }

    this.ingredientService.add(ingredient).subscribe(
      data => {
        this.ingredients.push(data);
        this.ingredientCreateResult = null;
        this.modalRef.hide();
      },
      error => this.ingredientCreateResult = 'Wystąpił nieoczekiwany błąd podczas dodawania składnika'
    );
  }

  updateIngredient() {
    const ingredient = new IngredientUpdateModel();
    try {
      ingredient.setId(this.tempIngredientUpdate.id);
      ingredient.setIngredientName(this.tempIngredientUpdate.ingredientName);
      ingredient.setIsAnimalProduct(this.tempIngredientUpdate.animalProduct);
    } catch (e) {
      this.ingredientUpdateResult = e.message;
      return;
    }

    this.ingredientService.update(ingredient).subscribe(
      data => {
        const editedIngredientId = this.ingredients.findIndex(i => i.id === data.id);
        this.ingredients.splice(editedIngredientId, 1);
        this.ingredients.push(data);
        this.ingredientUpdateResult = null;
        this.modalRef.hide();
      },
      error => this.ingredientUpdateResult = 'Wystąpił nieoczekiwany błąd podczas edycji składnika'
    );
  }


}
