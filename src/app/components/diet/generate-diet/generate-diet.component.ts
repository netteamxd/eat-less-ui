import { Component, OnInit } from '@angular/core';
import { DietInfoModel } from '../../../../models/diet/DietInfoModel';
import { DietService } from '../../../../services/diet.service';
import { AlertService } from '../../../../services/alert.service';
import { DietModel } from '../../../../models/diet/DietModel';
import { Gender } from '../../../../models/diet/GenderEnum';
import { Activity } from '../../../../models/diet/ActivityEnum';

@Component({
  selector: 'app-generate-diet',
  templateUrl: './generate-diet.component.html',
  styleUrls: ['./generate-diet.component.css']
})
export class GenerateDietComponent implements OnInit {
  activity = [
    { id: 'NONE', name: 'Brak' },
    { id: 'LOW', name: 'Mała' },
    { id: 'MODERATE', name: 'Średnia' },
    { id: 'HIGH', name: 'Duża' },
    { id: 'VERY_HIGH', name: 'P O T Ę Ż N A' }
  ];
  gender = [
    { id: 'MALE', name: 'Mężczyzna' },
    { id: 'FEMALE', name: 'Kobieta' }
  ];
  dietGenerateResult: any;
  tempDietInfo: DietInfoModel = new DietInfoModel();
  generatedDiet: DietModel;
  constructor(private dietService: DietService, private alertService: AlertService) { }

  ngOnInit() {
    this.tempDietInfo.age = 18;
    this.tempDietInfo.height = 180;
    this.tempDietInfo.mass = 80;
    this.tempDietInfo.gender = Gender.MALE;
    this.tempDietInfo.activity = Activity.MODERATE;
  }

  generate() {
    const dietInfo = new DietInfoModel();
    this.dietGenerateResult = null;
    try {
      dietInfo.setMass(this.tempDietInfo.mass);
      dietInfo.setHeight(this.tempDietInfo.height);
      dietInfo.setAge(this.tempDietInfo.age);
      dietInfo.setActivity(this.tempDietInfo.activity);
      dietInfo.setGender(this.tempDietInfo.gender);
    } catch (e) {
      this.dietGenerateResult = e.message;
      return;
    }

    this.dietService.generate(dietInfo).subscribe(
      data => this.generatedDiet = data,
      error =>  { console.log(error);
      this.dietGenerateResult = 'Tworzenie diety nie powiodło się - nie znaleziono posiłków odpowiednich dla Ciebie'; }
    );
  }
}
