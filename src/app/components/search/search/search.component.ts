import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../../../services/recipe.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SearchModel } from '../../../../models/search/SearchModel';
import { AlertService } from '../../../../services/alert.service';
import { RecipeListModel } from '../../../../models/recipe/RecipeListModel';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  currentPage = 1;
  recipes: RecipeListModel[] = [];
  keywords: string[] = [];
  constructor(private recipeService: RecipeService, private route: ActivatedRoute,
              private alertService: AlertService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
        this.keywords = params.get('keywords').split(',');
        this.getRecipes();
    });
  }

  getRecipes() {
    const searchModel = new SearchModel();
    searchModel.nameKeywords = this.keywords;
    this.recipeService.search(searchModel).subscribe(
      data => this.recipes = data
    );
  }
}
