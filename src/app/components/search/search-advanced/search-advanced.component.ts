import { Component, OnInit } from '@angular/core';
import { SearchModel } from '../../../../models/search/SearchModel';
import { RecipeService } from '../../../../services/recipe.service';
import { RecipeListModel } from '../../../../models/recipe/RecipeListModel';
import { CategoryService } from '../../../../services/category.service';
import { CategoryListModel } from '../../../../models/category/CategoryListModel';
import { Observable } from 'rxjs/index';
import { IngredientListModel } from '../../../../models/ingredient/IngredientListModel';
import { IngredientService } from '../../../../services/ingredient.service';

@Component({
  selector: 'app-search-advanced',
  templateUrl: './search-advanced.component.html',
  styleUrls: ['./search-advanced.component.css']
})
export class SearchAdvancedComponent implements OnInit {
  protected categories$: Observable<CategoryListModel[]>;
  protected ingredients$: Observable<IngredientListModel[]>;
  recipes: RecipeListModel[] = [];
  searchModel: SearchModel = new SearchModel();
  constructor(private recipeService: RecipeService, private categoryService: CategoryService,
              private ingredientService: IngredientService) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getAll();
    this.ingredients$ = this.ingredientService.getAll();
  }

  search() {
    this.recipeService.search(this.searchModel).subscribe(
      data => this.recipes = data
    );
  }
}
