import { Component, OnInit } from '@angular/core';
import { IngredientService } from '../../../../services/ingredient.service';
import { UserService } from '../../../../services/user.service';
import { Observable } from 'rxjs/Observable';
import { IngredientListModel } from '../../../../models/ingredient/IngredientListModel';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  excluded: number[] = [];
  protected ingredients$: Observable<IngredientListModel[]>;
  constructor(private ingredientService: IngredientService, private userService: UserService) { }

  ngOnInit() {
    this.ingredients$ = this.ingredientService.getAll();
    this.userService.getExcludedIngredients().subscribe(
      data => {
        data.forEach(x => this.excluded = [...this.excluded, x.id]);
      },
    );
  }

  excludeIngredient($event) {
    this.userService.excludeIngredient($event['id']).subscribe();
  }

  approveIngredient($event) {
    this.userService.unexcludeIngredient($event['value']['id']).subscribe();
  }

}
