import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDetailsModel } from '../models/user/UserDetailsModel';
import { Observable } from 'rxjs/index';
import { IngredientListModel } from '../models/ingredient/IngredientListModel';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  public info(): Observable<UserDetailsModel> {
    return this.http.get<UserDetailsModel>('api/auth/info');
  }

  public addFavourite(id: number) {
    const data = {
      recipeId: id.toString()
    };
    return this.http.post('api/auth/favouriteRecipes', {}, { params: data });
  }

  public removeFavourite(id: number) {
    const data = {
      recipeId: id.toString()
    };
    return this.http.delete('api/auth/favouriteRecipes', { params: data });
  }

  public getExcludedIngredients(): Observable<IngredientListModel[]> {
    return this.http.get<IngredientListModel[]>('api/auth/excludedIngredients');
  }

  public excludeIngredient(id: number) {
    const data = {
      ingredientId: id.toString()
    };
    return this.http.post('api/auth/excludedIngredients', {}, { params: data });
  }

  public unexcludeIngredient(id: number) {
    const data = {
      ingredientId: id.toString()
    };
    return this.http.delete('api/auth/excludedIngredients', { params: data });
  }
}
