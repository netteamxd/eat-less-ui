import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs-compat/add/operator/catch';

@Injectable()
export class ImageService {

  constructor(private http: HttpClient) {}

  public uploadImage(file: File) {
    const formData: FormData = new FormData();
    formData.append('image', file, file.name);
    return this.http.post('api/images', formData, { responseType: 'text'});
  }
}
