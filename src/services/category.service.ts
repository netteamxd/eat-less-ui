import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CategoryListModel } from '../models/category/CategoryListModel';
import { CategoryDetailsModel } from '../models/category/CategoryDetailsModel';
import { CategoryUpdateModel } from '../models/category/CategoryUpdateModel';
import { CategoryCreationModel } from '../models/category/CategoryCreationModel';

@Injectable()
export class CategoryService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<CategoryListModel[]> {
    return this.http.get<CategoryListModel[]>('api/categories');
  }

  public add(category: CategoryCreationModel): Observable<CategoryDetailsModel> {
    const body = JSON.stringify(category);
    return this.http.post<CategoryDetailsModel>('api/categories', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      });
  }

  public update(category: CategoryUpdateModel): Observable<CategoryDetailsModel> {
    const body = JSON.stringify(category);
    console.log(body);
    return this.http.put<CategoryDetailsModel>('api/categories', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      });
  }

  public getDetails(id: number): Observable<CategoryDetailsModel> {
    return this.http.get<CategoryDetailsModel>('api/categories/' + id);
  }

  public getLowestCategories(): Observable<CategoryListModel[]> {
    return this.http.get<CategoryListModel[]>('api/categories/lowest');
  }

  public getCategoryGenealogy(id: number): Observable<CategoryListModel[]> {
    return this.http.get<CategoryListModel[]>('api/categories/origin/' + id);
  }
}
