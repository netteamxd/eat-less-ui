import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RecipeListModel } from '../models/recipe/RecipeListModel';
import { RecipeCreationModel } from '../models/recipe/RecipeCreationModel';
import { RecipeDetailsModel } from '../models/recipe/RecipeDetailsModel';
import { RecipeUpdateModel } from '../models/recipe/RecipeUpdateModel';
import { SearchModel } from '../models/search/SearchModel';

@Injectable()
export class RecipeService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<RecipeListModel[]> {
    return this.http.get<RecipeListModel[]>('api/recipes');
  }

  public add(recipe: RecipeCreationModel) {
    const body = JSON.stringify(recipe);
    return this.http.post('api/recipes', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
    });
  }

  public getDetails(id: number): Observable<RecipeDetailsModel> {
    return this.http.get<RecipeDetailsModel>('api/recipes/' + id);
  }

  public delete(id: number) {
    return this.http.delete('api/recipes/' + id);
  }

  public update(recipe: RecipeUpdateModel) {
    const body = JSON.stringify(recipe);
    return this.http.put('api/recipes', body, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    });
  }

  public search(search: SearchModel): Observable<RecipeListModel[]> {
    let searchParams = new HttpParams();
    if (typeof search.maxCalories !== 'undefined' && search.maxCalories) {
      searchParams = searchParams.append('maxCalories', search.maxCalories.toString());
    }
    if (typeof search.minCalories !== 'undefined' && search.minCalories) {
      searchParams = searchParams.append('minCalories', search.minCalories.toString());
    }
    if (search.ingredients.length > 0) {
      searchParams = searchParams.append('ingredients', search.ingredients.join(','));
    }
    if (search.excluded.length > 0) {
      searchParams = searchParams.append('excluded', search.excluded.join(','));
    }
    if (search.nameKeywords.length > 0) {
      searchParams = searchParams.append('nameKeywords', search.nameKeywords.join(','));
    }
    if (search.categories.length > 0) {
      searchParams = searchParams.append('categories', search.categories.join(','));
    }
    return this.http.get<RecipeListModel[]>('api/recipes/search', { params: searchParams });
  }

  public getRecommended(): Observable<RecipeListModel[]> {
    return this.http.get<RecipeListModel[]>('api/recipes/recommended');
  }
}
