import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReviewCreationModel } from '../models/review/ReviewCreationModel';
import { ReviewUpdateModel } from '../models/review/ReviewUpdateModel';


@Injectable()
export class ReviewService {
  constructor(private http: HttpClient) {}

  public add(review: ReviewCreationModel) {
    const body = JSON.stringify(review);
    return this.http.post('api/reviews', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
    });
  }

  public delete(id: number) {
    return this.http.delete('api/reviews/' + id);
  }

  public update(review: ReviewUpdateModel) {
    const body = JSON.stringify(review);
    console.log(body);
    return this.http.put('api/reviews', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      });
  }
}
