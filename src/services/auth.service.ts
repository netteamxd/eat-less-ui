import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserRegistrationModel } from '../models/user/UserRegistrationModel';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { AlertService } from './alert.service';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private alertService: AlertService,
              private router: Router, private userService: UserService) {}

  private authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(localStorage.getItem('username') !== null);
  isAuthenticated: Observable<boolean> = this.authenticated.asObservable();
  private admin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(AuthService.getAdminInitialValue());
  isAdmin: Observable<boolean> = this.admin.asObservable();

  static getAdminInitialValue(): boolean {
    const isAdminLocalStorage = localStorage.getItem('admin');
    return isAdminLocalStorage !== null && isAdminLocalStorage === 'true';
  }

  register(user: UserRegistrationModel) {
    const userData = {
      username: user.username,
      password: user.password
    };
    this.http.post('api/auth/register', {}, { params: userData })
      .subscribe(
        data => {
          this.alertService.success('Konto zostało pomyślnie założone. Zaloguj się używając swojego loginu i hasła.', true);
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error('Taki użytkownik już istnieje.');
        }
      );
  }

  login(user: UserRegistrationModel) {
    const userData = {
      username: user.username,
      password: user.password
    };
    this.http.post('api/auth/login', {}, { params: userData })
      .subscribe(
        data => {
          this.getUserData();
          this.authenticated.next(true);
          this.router.navigate(['/']);
        },
        error => {
          this.alertService.error('Błędny login lub hasło.');
        }
      );
  }

  getUserData() {
    this.userService.info().subscribe(
      data => {
        localStorage.setItem('username', data.username);
        localStorage.setItem('admin', data.admin ? 'true' : 'false');
        this.admin.next(data.admin);
      });
  }

  logout() {
    this.http.post('api/auth/logout', {}).subscribe(
      data => {
        localStorage.clear();
        this.authenticated.next(false);
        this.admin.next(false);
        this.router.navigate(['/']);
    });
  }
}
