import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IngredientListModel } from '../models/ingredient/IngredientListModel';
import { IngredientCreationModel } from '../models/ingredient/IngredientCreationModel';
import { IngredientDetailsModel } from '../models/ingredient/IngredientDetailsModel';
import { IngredientUpdateModel } from '../models/ingredient/IngredientUpdateModel';

@Injectable()
export class IngredientService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<IngredientListModel[]> {
    return this.http.get<IngredientListModel[]>('api/ingredients').map(
      (data) => {
        data.sort((a, b) => a.ingredientName.localeCompare(b.ingredientName));
        return data;
      }
    );
  }

  public add(ingredient: IngredientCreationModel): Observable<IngredientDetailsModel> {
    const body = JSON.stringify(ingredient);
    return this.http.post<IngredientDetailsModel>('api/ingredients', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      });
  }

  public update(ingredient: IngredientUpdateModel): Observable<IngredientDetailsModel> {
    const body = JSON.stringify(ingredient);
    console.log(body);
    return this.http.put<IngredientDetailsModel>('api/ingredients', body,
      {
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      });
  }
}
