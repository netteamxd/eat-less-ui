import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DietInfoModel } from '../models/diet/DietInfoModel';
import { Observable } from 'rxjs/Observable';
import { DietModel } from '../models/diet/DietModel';

@Injectable()
export class DietService {
  constructor(private http: HttpClient) {}

  public generate(dietInfoModel: DietInfoModel): Observable<DietModel> {
    const body = JSON.stringify(dietInfoModel);
    return this.http.post<DietModel>('api/diet', body, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    });
  }
}
